.. Nomenclature API documentation master file, created by
   sphinx-quickstart on Fri Oct 02 16:40:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Документация Nomenclature API
=============================

Данный документ содержит описание основных механизмов работы сервиса и модулей и не содержит подробное описание реализации. За подробным техническим описанием необходимо обращаться к тестам и непосредственно к коду.

.. toctree::
   :maxdepth: 2

   fundamentals/index.rst
   dal/index.rst
   cqrs/index.rst
   domain/index.rst
   api/index.rst
   tests/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`