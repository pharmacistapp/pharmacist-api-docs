CQRS
====

Библиотека основаная на принципе `command-query responsibility segregation (CQRS) <https://ru.wikipedia.org/wiki/CQRS>`_.

Кратко принцип означает, что метод должен быть либо командой, выполняющей какое-то действие, либо запросом, возвращающим данные, но не одновременно.

.. attention:: На данный момент библиотека реализовывает только запросы.

IQueryBuilder
_____________

Элемент для вызова запросов в системе. Данный интерфейс реализован в библиотеке и зарегестрирован в DI модуле.
Интерфейс выглядит следующим образом:

.. code-block:: csharp

  namespace NomenclatureApi.CQRS
  {
      public interface IQueryBuilder
      {
          IQueryFor<TResult> For<TResult>();
      }
  }

Пример получения и использования:

.. code-block:: csharp

  using NomenclatureApi.CQRS

  namespace Logic
  {
    public class LogicClass
    {

      private IQueryBuilder _queryBuilder;

      public LogicClass(IQueryBuilder queryBulider)
      {
        this._queryBuilder = queryBulider;
      }

      public void Method()
      {
        var queryFor = _queryBuilder.For<int>();
        ...
      }
    }
  }

IQueryFor<TResult>
__________________

Вспомогательный интерфейс для выполнения запросов с необходимыми параметрами

.. code-block:: csharp

  namespace NomenclatureApi.CQRS
  {
      public interface IQueryFor<out TResult>
      {
          TResult With<TCriterion>(TCriterion criterion);
      }
  }

IQuery<TCriterion, TResult>
___________________________

Основной логический интерфейс, который необходимо реализовывать в бизнес коде приложения. Класс реализующий этот интерфейс инкапсулирует в себе отдельный запрос.

.. code-block:: csharp

  namespace NomenclatureApi.CQRS
  {
      public interface IQuery<in TCriterion, out TResult>
      {
          TResult Ask(TCriterion criterion);
      }
  }

* TCriterion - тип условия (параметра) запроса
* TResult - результат выполнения запроса

Пример использования
____________________

Данный пример иллюстрирует использование всех элементов библиотеки (код не включает подключение всех пространств имен и не разделен на отдельные файлы)

.. code-block:: csharp

  // Условие для запроса
  public class QueryFilter
  {
    public string Filter { get; set; }
  }

  // Класс для результата ответа
  public class CountResult
  {
    public int Count { get; set; }
  }

  // Класс реализующий запрос
  public class CountByQueryFilterQuery : IQuery<QueryFilter, CountResult>
  {
    public CountResult Ask(QueryFilter criterion)
    {
      var filter = criterion.Filter;
      
      var count = // логика приложения

      var result = new CountResult
      {
        Count = count
      };

      return result;
    }
  }

  // Класс которому необходим запрос CountResult
  public class AnotherBusinessClass
  {
    private readonly IQueryBuilder queryBuilder;

    // получение зависимости через DI
    public AnotherBusinessClass(IQueryBuilder queryBuilder)
    {
      this.queryBuilder = queryBuilder;
    }

    public bool CheckCount()
    {
      // Создание объекта условия
      QueryFilter queryFilter = new QueryFilter { Filter = "code = 200" };

      // Вызов запроса с динамическим нахождением типа класса запроса
      CountResult countResult = queryBuilder.For<CountResult>().With(queryFilter);

      // обработка результата
      return countResult.Count > 100;
    }
  }

Подключение библиотеки
______________________

Библиотека регистрируется с помощью DI модуля:

.. code-block:: csharp

  using Autofac;
  using NomenclatureApi.CQRS;
  ...

  var builder = new ContainerBuilder();
  builder.RegisterModule<CqrsModule>();