Связь модулей через Dependency Injection
========================================

Регистрация всех модулей происходит в основном приложении ASP.NET. В классе ``WebApiApplication``

.. code-block:: csharp

  public class WebApiApplication : System.Web.HttpApplication
  {
      ...
      private void RegisterApplicationModules()
      {
        ...

        builder.RegisterModule<CQRS.CqrsModule>();
        builder.RegisterModule<DAL.DALModule>();
        builder.RegisterModule<Domain.DomainModule>();
        
        ...
      }
  }

Сам модуль представляет собой класс потомок класса ``Autofac.Module``, который регистрирует зависимости своего уровня необходимые в приложении.

.. code-block:: csharp

  using Autofac;

  namespace NomenclatureApi.DAL
  {
      public class DALModule : Module
      {
          protected override void Load(ContainerBuilder builder)
          {
              builder.Register(c => new RlsDbContext())
                  .As<ITradenameDbContext>()
                  .InstancePerLifetimeScope();
          }
      }
  }

В данном примере происходит регистрация контекста базы данных Entity Framework ``RlsDbContext`` представляемого как интерфейс ``ITradenameDbContext``.