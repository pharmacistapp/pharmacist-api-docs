DAL - Уровень данных
====================

Модуль содержит в себе описание моделей базы данных, интерфейса контекста доступа к базе данных ``ITradenameDbContext``.
Модуль регистрирует реализацию контекста БД EntityFramework в качестве интерфейса ``ITradenameDbContext``.

Модели базы данных
__________________

* Торговые названия ``Tradenames``
* Версии базы данных ``DatabaseVersions``
* Формы выпуска торговых названий ``TradenameDrugforms``
* Номеклатуры ``Nomenclatures``
* Описания номенклатур ``NomenclatureDescriptions``
* Аналоги торговых названий ``TradenameAnalogues``
* Действующие вещества торговых названий ``TradenameMaterials``
* Описания действующих веществ ``MaterialDescriptions``
* Описания торговых названий ``TradenameDescriptions``
* Дозировки торговых названий ``TradenameDosages``
* Взаимодействие действующих веществ ``MaterialInteractions``
* Поиск номенклатур по вхождению имени ``TradenameSearch(string filter)``